const express = require('express');
const helper = require('../common/commonHelper');
const GetInfoDTO = require('../dtos/get-info-dto');
const GetInfoController = require('../controllers/get-info-controller');

const router = express.Router();

function handler(request, response) {
  let getInfoDTO;
  let toReturn;
  try {
    getInfoDTO = new GetInfoDTO(request.params.id);
  } catch (err) {
    helper.setResponseWithError(response, 404, 'Not Found');
  }
  const getInfoController = new GetInfoController();
  getInfoController.getInfo(getInfoDTO)
  .then((result) => {
    toReturn = helper.setResponse(response, result);
  }).catch((error) => {
    toReturn = helper.setResponse(response, error);
  });
  return toReturn;
}

router.get('/info/:id', handler);

module.exports = router;
module.exports.handler = handler;
