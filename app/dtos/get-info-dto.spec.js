const GetInfoDTO = require("./get-info-dto");
describe("GetInfoDTO DTO", () => {
 
  let getInfoDTO = null;

  beforeEach( () => {    
    
  });

  afterEach( () => {
    getInfoDTO = null;
  });
  
  it("should be an instance of getInfoDTO ", () => {    
    getInfoDTO = new GetInfoDTO();
    expect(getInfoDTO instanceof GetInfoDTO).toBe(true);    
  });

  describe("attribute id", () => {

    it("should throw an exception", () => {
      try{                                   
        getInfoDTO = new GetInfoDTO({id:"sasa"});
      } catch (err) {

        expect(err.name).toBe("InvalidPropertyError");               
        expect(err.message).toBe("Property of type number required");               
      }  
    });

    it("should return the correct value", () => {
      try{                                   
        getInfoDTO = new GetInfoDTO({id:1221});
        expect(getInfoDTO.id).toBeTruthy();               
      } catch (err) {
        throw  "should be in try";
      }  
    });     

  });  

});

