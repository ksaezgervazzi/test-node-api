process.env.NODE_ENV = 'iso';

// var request = require('request');
const assert = require('cucumber-assert');
const config = require('../../../../app/config/config.iso');
const Client = require('node-rest-client').Client;

// para mockear mongo
const mockery = require("mockery");

mockery.enable({
  warnOnReplace: false, 
  warnOnUnregistered: false, 
  useCleanCache: true
});

// mock de la funcion init mongo
mockery.registerMock('./init-mongo', () => {});

// Mock de mongoose
function MongooseMock () {}
MongooseMock.Schema = function() {
  this.plugin = function() {}
};
MongooseMock.model = function() {}

mockery.registerMock('mongoose', MongooseMock);


function isInt(value) {
  return !isNaN(value) && 
         parseInt(Number(value)) == value && 
         !isNaN(parseInt(value, 10));
}

const isoStepDefinitions = function () {
  const apiResponse = {};
  let requestHeaders = {};
  let request = {};
  
  this.Given(/^I start the API with ISO configuration$/, function (callback) {
    require("../../../../bin/www");
    callback(null, 'done');
  });

  this.Given(/^I set the correct request header$/, function (callback) {
    requestHeaders = {
      'x-access-token': config.accessToken,
      "Content-Type": "application/json"
    };
    
    callback(null, 'done');
  });

  this.Given(/^I set the wrong request header$/, function (callback) {
    requestHeaders = {
      'x-access-token': "some-bad-token", 
      "Content-Type": "application/json"
    };

    callback(null, 'done');
  });

  this.Given(/^I set the following header$/, function (table, callback) {
    const hashes = table.hashes();

    for (let i=0; i<hashes.length; i++) {
      let key = hashes[i].key;
      let value = hashes[i].value.trim(); 

      if (isInt(value)) {
        value = parseInt(value);
      }

      requestHeaders[key] = value; 
    }

    callback(null, 'done');
  });

  this.Given(/^I set the following body$/, function (table, callback) {
    request = {};

    const hashes = table.hashes();

    for (let i=0; i<hashes.length; i++) {
      let key = hashes[i].key;
      let value = hashes[i].value.trim(); 

      // console.log(`'${key}' = '${value}'`);

      if (isInt(value)) {
        value = parseInt(value);
      }

      request[key] = value; 
    }

    callback(null, 'done');
  });

  this.Given(/^I set the correct body$/, function (callback) {    
    request = {
      id : 1,
      name : "Lala",
      lastName : "Lolo",
      email : "mail@gmail.com",
      test : "11.111.111-1"
    };

    callback(null, 'done');
  });

  this.When(/^I call the following endpoint "([^"]*)"$/, function (url, callback) {
    const client = new Client();
    const args = {
      data: request,
      headers: requestHeaders,
    };

    // console.log(JSON.stringify(args));

    client.post(url, args, (data, response) => {
      apiResponse.statusCode = response.statusCode;
      apiResponse.statusMessage = response.statusMessage;
      apiResponse.data = data;
      callback(null, 'done');
    }).on('error', (err) => {
      throw err;
    });
  }); 


  this.Then(/^I see the following result for the response$/, function (table, callback) {
    const statusCode = apiResponse.statusCode;
    const statusMessage = apiResponse.statusMessage;

    const dataStatusCode = table.hashes()[0].statusCode;
    const dataStatusMessage = table.hashes()[0].statusMessage;
    
    const promises = [
      assert.equal(statusCode, dataStatusCode, `Expected the status code to be ${dataStatusCode}`), 
      assert.equal(statusMessage, dataStatusMessage, `Expected the status message to be ${dataStatusMessage}`)
    ];

    // console.log(JSON.stringify(apiResponse));
    // console.log(JSON.stringify(request));

    assert.all(promises).then(callback, callback);
  });
};

module.exports = isoStepDefinitions;
