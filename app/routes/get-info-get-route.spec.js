var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require("events").EventEmitter;

describe('getInfoGet route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('../../loggers/logger', {
      debug: function () {}, 
      error: function() {}, 
      info: function() {}
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('Test Handler Function', () => {
    it('should not have errors', () => {
      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      let request = httpMocks.createRequest({
        url: "/", 
        params: { "id": 1 },
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"}
      });

      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      route = require("./get-info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
    });

    it("should return an error when something is wrong with mongo", () => {

     class GetInfoDTOMock {
      constructor(params) {
        throw "Params error";
      }
    }

    mockery.registerMock('../dtos/get-info-dto', GetInfoDTOMock);
    
    class GetInfoControllerMock {
      getInfo(getInfoDTO) {
        return new Promise((resolve, reject) => {
          resolve({field1:"ok", field2: "ok"});
        });
      }
    }

    mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

    let request = httpMocks.createRequest({
      url: "/", 
      params: { "id": 1 },
      headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"}
    });

    let response = httpMocks.createResponse({
      eventEmitter: EventEmitter
    });

    route = require("./get-info-get-route");
    route.handler(request, response);

    expect(response.statusCode).toBe(404);
  });

    it("should return an error when the controller fails", () => {

      class GetInfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            reject({});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      let request = httpMocks.createRequest({
        url: "/",
        params: { "id": 1 }, 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"}
      });

      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      route = require("./get-info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
    });
  });
});