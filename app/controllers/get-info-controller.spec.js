var mockery = require('mockery');
var sinon = require('sinon');

describe("GetInfoController module", () => {
  let GetInfoController = null;
  let getInfoController = null;
  let originalTimeout;

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../loggers/logger.js', {
      info: (param, param2) => {},
      debug: (param) => {}}
    );

    mockery.registerMock('../../config/index', {});
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;          
  });

  afterEach(() => {
    getInfoController = null;
    getInfoController = null;
    mockery.disable();
    mockery.deregisterAll();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it("Should be an instance of GetInfoController", () => {                                  
    GetInfoController = require('./get-info-controller');
    getInfoController = new GetInfoController();                  
    expect(getInfoController instanceof GetInfoController).toBeTruthy();     
  });

  describe("getInfo method", () => {
    it("should return a promise ", (done) => {
      let checkObject = {used: () => {}}; 

      class InfoMongoServiceMock {
        getInfo(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            resolve({status:"ok", response:"ok"});
          });
        }
      }
      
      let checkObjectSpy = sinon.spy(checkObject, 'used'); 
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);
      
      GetInfoController = require('./get-info-controller');
      getInfoController = new GetInfoController();               
      let Info = require('../models/info-document');
      let info =  new Info({id:"1"});

      getInfoController.getInfo(info).then((response) => {
        expect(response.status).toBe("ok");
        expect(response.response).toBe("ok");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      })
      .catch((error) => {
        throw "should be in the then block";
      });
    });

    it("should return an error ", (done) => {
      let checkObject = {used: () => {}}; 

      class InfoMongoServiceMock {
        getInfo(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            reject({status, response});
          });
        }
      }
      let checkObjectSpy = sinon.spy(checkObject, 'used'); 
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);
      
      getInfoController = require('./get-info-controller');
      getInfoController = new GetInfoController();
      let Info = require('../models/info-document');
      let info =  new Info({id:"1"});

      getInfoController.getInfo(info).then((response) => {
        throw "should be in the catch block";
      })
      .catch((error) => {
        expect(error.status).toBeUndefined();
        expect(error.response).toBeUndefined();
        expect(checkObjectSpy.callCount).toBe(0);
        done();
      });
    });
  });

  describe("setId method", () => {
    it("should return an object with the correct information", () => {                               
      let Info = require('../models/info-document');
      let info =  new Info({id:"1"});

      GetInfoController = require('./get-info-controller');
      getInfoController = new GetInfoController();
      let object = getInfoController.setId(info);

      expect(object.id).toBe(info.id);            
    });

    it("the returned object should have 1 properties", () => {
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com"});

      GetInfoController = require('./get-info-controller');
      getInfoController = new GetInfoController();

      let object = getInfoController.setId(info);
      let size = 0;    
      for (key in object) {
          if (object.hasOwnProperty(key)) size++;
      }
      expect(size).toBe(1);
    });
  });
});
