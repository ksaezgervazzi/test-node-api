const InfoMongoService = require('../services/persistence/info-mongo-service');

module.exports = class GetInfoController {
  getInfo(getInfoDTO) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      const document = this.setId(getInfoDTO);
      infoMongoService.getInfo(document)
      .then((doc) => {
        resolve(doc);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }

  setId(dto) {
    const element = {};
    element.id = dto.id;
    return element;
  }

};
